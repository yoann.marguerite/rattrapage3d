using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class FormSend1 : MonoBehaviour
{
    public GameObject EmailInput;
    public GameObject FirstNameInput;
    public GameObject NameInput;
    public GameObject LabelAlert;

    private const string BASE_URL = "https://api.hubapi.com";
    private const string CONTACTS_URL = BASE_URL + "/crm/v3/objects/contacts";
    private const string HAPI_KEY = "eu1-7362-cb59-45d2-82e4-d194d6cc9232";

    public void Click()
    {
        if (EmailInput.GetComponent<InputField>().text == "" || NameInput.GetComponent<InputField>().text == "" || FirstNameInput.GetComponent<InputField>().text == "") return;
        RootProperties rootProperties = new RootProperties();
        Properties properties = new Properties();
        properties.email = EmailInput.GetComponent<InputField>().text;
        properties.lastname = NameInput.GetComponent<InputField>().text;
        properties.firstname = FirstNameInput.GetComponent<InputField>().text;
        rootProperties.properties = properties;
        string json = Newtonsoft.Json.JsonConvert.SerializeObject(rootProperties);
        string url = CONTACTS_URL + "?hapikey=" + HAPI_KEY;
        StartCoroutine(PostData(url, json));
    }

    public IEnumerator PostData(string url, string bodyJsonString)
    {
        var request = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyJsonString);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.Success)
        {
            Text labelAlert = LabelAlert.GetComponent<Text>();
            labelAlert.text = "Utilisateur ajout�";
            labelAlert.color = Color.green;
        }
        else
        {
            Text labelAlert = LabelAlert.GetComponent<Text>();
            labelAlert.text = "La requ�te n'a pas aboutie";
            labelAlert.color = Color.red;
        }
    }
}


