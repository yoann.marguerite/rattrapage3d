using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class FormSend : MonoBehaviour
{
    public GameObject EmailInput;
    public GameObject FirstNameInput;
    public GameObject NameInput;
    public GameObject LabelAlert;

    private const string BASE_URL = "https://api.hubapi.com";
    private const string CONTACTS_URL = BASE_URL + "/crm/v3/objects/contacts";
    private const string HAPI_KEY = "eu1-7362-cb59-45d2-82e4-d194d6cc9232";

    public void Click()
    {
        if (EmailInput.GetComponent<InputField>().text == "" || NameInput.GetComponent<InputField>().text == "" || FirstNameInput.GetComponent<InputField>().text == "") return;
        RootProperties rootProperties = new RootProperties();
        Properties properties = new Properties();
        properties.email = EmailInput.GetComponent<InputField>().text;
        properties.lastname = NameInput.GetComponent<InputField>().text;
        properties.firstname = FirstNameInput.GetComponent<InputField>().text;
        rootProperties.properties = properties;

        string json = Newtonsoft.Json.JsonConvert.SerializeObject(rootProperties);
        string url = CreateUrl(CONTACTS_URL, HAPI_KEY);

        PostData(url, json);
    }

    public void PostData(string url, string bodyJsonString)
    {
        var httpWebRequest = CreateRequest(url);

        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        {
            streamWriter.Write(bodyJsonString);
        }

        Text labelAlert = LabelAlert.GetComponent<Text>();
        try
        {
            var httpResponse = (HttpWebResponse)SendRequest(bodyJsonString, httpWebRequest).GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                print("Utilisateur ajout�");
                labelAlert.text = "Utilisateur ajout�";
                labelAlert.color = Color.green;
            }
        }
        catch (WebException e)
        {
            print(e.Message);
            print("La requ�te n'a pas aboutie");
            labelAlert.text = "La requ�te n'a pas aboutie";
            labelAlert.color = Color.red;
        }
    }

    public static string CreateUrl(string url, string param)
    {
        return url + "?hapikey=" + param;
    }

    public static HttpWebRequest CreateRequest(string url)
    {
        var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Method = "POST";
        return httpWebRequest;
    }

    public static HttpWebRequest SendRequest(string bodyJsonString, HttpWebRequest httpWebRequest)
    {
        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        {
            streamWriter.Write(bodyJsonString);
        }

        return httpWebRequest;
    }
}
