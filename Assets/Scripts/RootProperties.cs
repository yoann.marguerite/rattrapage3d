﻿using System.Collections;
using UnityEngine;

public class RootProperties
{
    public Properties properties { get; set; }
}

public class Properties
{
    public string company { get; set; }
    public string email { get; set; }
    public string firstname { get; set; }
    public string lastname { get; set; }
    public string phone { get; set; }
    public string website { get; set; }
}