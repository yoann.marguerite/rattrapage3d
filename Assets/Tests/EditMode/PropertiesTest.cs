using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class PropertiesTest
{
    RootProperties rootProperties = null;
    Properties properties = null;
    string name = "Name";
    string firstname = "FirstName";
    string email = "Mail";

    [SetUp]
    public void SetUp()
    {
        rootProperties = new RootProperties();
        properties = new Properties();
        rootProperties.properties = properties;
    }

    [Test]
    public void Instantiate()
    {
        Assert.AreNotEqual(rootProperties, null);
        Assert.AreNotEqual(properties, null);
        Assert.AreEqual(rootProperties.properties, properties);
    }

    [Test]
    public void Properties()
    {
        rootProperties.properties.lastname = name;
        rootProperties.properties.firstname = firstname;
        rootProperties.properties.email = email;
        Assert.AreEqual(rootProperties.properties.lastname, name);
        Assert.AreEqual(rootProperties.properties.firstname, firstname);
        Assert.AreEqual(rootProperties.properties.email, email);
    }
}
