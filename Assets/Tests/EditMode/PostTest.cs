using Newtonsoft.Json;
using NUnit.Framework;

public class PostTest
{

    private const string BASE_URL = "https://api.hubapi.com";
    private const string CONTACTS_URL = BASE_URL + "/crm/v3/objects/contacts";
    private const string HAPI_KEY = "eu1-7362-cb59-45d2-82e4-d194d6cc9232";

    [Test]
    public void UrlTest()
    {
        Assert.AreEqual(FormSend.CreateUrl(CONTACTS_URL, HAPI_KEY), CONTACTS_URL + "?hapikey=" + HAPI_KEY);
    }

    [Test]
    public void CreateTest()
    {
        var http = FormSend.CreateRequest(FormSend.CreateUrl(CONTACTS_URL, HAPI_KEY));

        Assert.IsNotNull(http);
    }

    [Test]
    public void SendTest()
    {
        RootProperties rootProperties = new RootProperties();
        Properties properties = new Properties();
        properties.email = "test@gmail.com";
        properties.lastname = "test";
        properties.firstname = "test";
        rootProperties.properties = properties;

        string url = FormSend.CreateUrl(CONTACTS_URL, HAPI_KEY);
        string json = JsonConvert.SerializeObject(rootProperties);

        var http = FormSend.CreateRequest(url);

        var request = FormSend.SendRequest(json, http);

        Assert.IsNotNull(request);
    }
}
